// jquery 

$(document).ready(function(){

    // ketika div dengan id hideLogin di klik maka div dengan id login form di hide 
    // dan div dengan id registerForm di show

    $("#hideLogin").click(function(){
        $("#loginForm").hide();
        $("#registerForm").show();
    })

    // ketika div dengan id hideRegister di klik maka div dengan #login form di perlihatkan dan registerform di hide
    $("#hideRegister").click(function(){
        $("#registerForm").hide();
        $("#loginForm").show();
        
    })
});