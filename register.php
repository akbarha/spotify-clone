<?php 
    /* 
        require dan includes sebenarnya hanya menyatukan kode di file berbeda menjadi 1  sehingga data/variabel/fungsi bisa di file berbeda bisa dipanggil
    */

    // koneksi ke mysql
    require("includes/config.php");

    // Constants Error Messages
    require("includes/classes/Constants.php");

    // Classes Account dimana data register form di validasi dan dilakukan registering dan login account
    require("includes/classes/Account.php");
    $account = new Account($con); // pass objek koneksi ke class account

    // handler register ketika tombol submit form register di klik. disini ditaruh logika logika pemrograman nya
    require("includes/handlers/register-handler.php");

    // handler login ketika tombol login form login di klik. disini ditaruh logika pemrograman nya
    require("includes/handlers/login-handler.php");

    // fungsi buat mengingat apa yang sudah user input di register form, jadi ketika salah tidak harus mengisi form dari awal lagi.

    function getInputValue($name){
        if(isset($_POST[$name])){
            echo $_POST[$name];
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome to Slotify!</title>
    <!-- css -->
    <!-- Include the Google Fonts CSS -->
    <link href="https://fonts.googleapis.com/css?family=Helvetica+Neue&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/register.css">

    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="assets/js/register.js"></script>
</head>

<body>
    <!-- show/hide login register form jquery and custom js-->
    <?php 
        if(isset($_POST['registerButton'])){
            echo '<script>
                    $(document).ready(function(){
                        $("#loginForm").hide();
                        $("#registerForm").show();
                    })
                  </script>';
        }
        else{
            echo '<script>
            $(document).ready(function(){
                $("#loginForm").show();
                $("#registerForm").hide();
            })
          </script>';
        }
    ?>



    <div id="background">
        <div id="loginContainer">
            <div id="inputContainer">
                <form action="register.php" id="loginForm" method="POST">
                    <h2>Login to your account</h2>

                    <?php echo $account->getError(Constants::$loginFailed) ?>

                    <p>
                        <label for="loginUsername">Username</label>
                        <input type="text" name="loginUsername" id="loginUsername" required>
                    </p>

                    <p>
                        <label for="loginPassword">Password</label>
                        <input type="password" name="loginPassword" id="loginPassword"  required>
                    </p>

                    <button type="submit" name="loginButton">Log In</button>

                    <div class="hasAccountText">
                        <span id="hideLogin">Don't have an account yet? Sign up here.</span>
                    </div>


                </form>

                <form action="register.php" id="registerForm" method="POST">
                    <h2>Create a free account</h2>
                    <!-- check error array -->
                    <?php echo $account->getError(Constants::$usernameCharacters) ?>
                    <?php echo $account->getError(Constants::$usernameTaken) ?>
                    <p>
                        <label for="username">Username</label>
                        <input type="text" name="username" id="username" placeholder="e.g. bart420mlg" required value="<?php getInputValue('name') ?>">
                    </p>

                    <?php echo $account->getError(Constants::$firstNameCharacters) ?>

                    <p>
                        <label for="firstName">First Name</label>
                        <input type="text" name="firstName" id="firstName" placeholder="e.g. Bart" required value="<?php getInputValue('firstName') ?>">
                    </p>

                    <?php echo $account->getError(Constants::$lastNameCharacters) ?>
                    <p>
                        <label for="lastName">Last Name</label>
                        <input type="text" name="lastName" id="lastName" placeholder="e.g. Simpson" required value="<?php getInputValue('lastName') ?>">
                    </p>

                    <?php echo $account->getError(Constants::$emailInvalid) ?>

                    <?php echo $account->getError(Constants::$emailsDonNotMatch) ?>
                    <?php echo $account->getError(Constants::$emailTaken) ?>

                    <p>
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" placeholder="e.g. bart@somemegacorpo.com" required value="<?php getInputValue('email') ?>">
                    </p>

                    <p>
                        <label for="email2">Confirm Email</label>
                        <input type="email" name="email2" id="email" placeholder="e.g. bart@somemegacorpo.com" required value="<?php getInputValue('email2') ?>">
                    </p>

                        <?php echo $account->getError(Constants::$passwordDoNotMatch) ?>

                        <?php echo $account->getError(Constants::$passwordNotAlphaNumeric) ?>

                        <?php echo $account->getError(Constants::$usernameCharacters) ?>

                    <p>
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password"  required placeholder="Your Password">
                    </p>

                    <p>
                        <label for="password2">Confirm Password</label>
                        <input type="password" name="password2" id="password2"  required placeholder=" Confirm Your Password">
                    </p>

                    <button type="submit" name="registerButton">Register</button>

                    <div class="hasAccountText">
                        <span id="hideRegister">Already have an account? Log in here.</span>
                    </div>

                </form>
            </div>

            <div id="loginText">
                <h1>Get great music, right now</h1>
                <h2>Listen to loads of songs for free</h2>
                <ul>
                    <li>Discover music you'll fall in love with</li>
                    <li>Create your own playlist</li>
                    <li>Follow artists to keeup up to date</li>
                </ul>
            </div>

            
        </div>
    </div>

</body>
</html>