<div id="navBarContainer">
                <!-- navbar display flex -->
                <nav class="navbar">
                    <a href="index.php" class="logo">
                        <img src="assets/icons/logo.png" alt="">
                    </a>

                    <div class="group">
                        <div class="navItem">
                            <a href="search.php" class="navItemLink">Search
                                <img src="assets/icons/search.png" alt="Search" class="icon">
                            </a>
                        </div>
                    </div>

                    <div class="group">
                        <div class="navItem">
                            <a href="browse.php" class="navItemLink">Browser</a>
                        </div>

                        <div class="navItem">
                            <a href="yourMusic.php" class="navItemLink">Your Music</a>
                        </div>

                        <div class="navItem">
                            <a href="profile.php" class="navItemLink">Reece Kenney</a>
                        </div>
                    </div>
                </nav>
            </div>