<?php 
// Login Handler
if(isset($_POST['loginButton'])){
    // Login button ditekan
    $username = $_POST['loginUsername'];
    $password = $_POST['loginPassword'];

    // panggil fungsi login
    $result = $account->login($username, $password);
    if($result == true){
        $_SESSION['userLoggedIn'] = $username;
        header("Location: index.php");
    }
}