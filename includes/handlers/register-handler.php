<?php
    // create funciton sanitizer untuk data form
    function sanitizeFormUsername($inputText){
        // clear html tags dalam form jika user submit dengan html tags !
        $inputText = strip_tags($inputText);
        // replace spasi dengan kosong
        $inputText = str_replace(" ", "", $inputText);
        return $inputText;
    }

    function sanitizeFormString($inputText){
        $inputText = strip_tags($inputText);
        $inputText = str_replace(" ", "", $inputText);
        // lowercase kemudian uppercase!
        $inputText = ucfirst(strtolower($inputText));
        return $inputText;
    }

    function sanitizeFormEmail($inputText){
        $inputText = strip_tags($inputText);
        $inputText = str_replace(" ", "",   $inputText);
        return $inputText;
    }

    function sanitizeFormPassword($inputText){
        $inputText=strip_tags($inputText);
        return $inputText;
    }
    // End Of Sanitizer Function

    if(isset($_POST['registerButton'])){
        // echo "Register Button was Pressed!";
        // ketika button register di klik maka :

        // get username via associative array
        $username = $_POST['username'];
        // panggil fungsi sanitasi data teks
        $username = sanitizeFormUsername($username);

        $firstName = $_POST['firstName'];
        // panggil fungsi sanitasi string untuk firstName
        $firstName = sanitizeFormString($firstName);

        // lakukan hal yang sama untuk data form lainnya
        $lastName = sanitizeFormString($_POST['lastName']);

        $email = sanitizeFormEmail($_POST['email']);

        $email2 = sanitizeFormEmail($_POST['email2']);

        $password = sanitizeFormPassword($_POST['password']);

        $password2 = sanitizeFormPassword($_POST['password2']);

         // Setelah disanitasi maka validasi

         // Validation Function , Dibuat Ke Sebuah Class (Object Oriented Programming Ada di classes/Account.php)

        //  validasi berhasil? panggil fungsi register dari object $account
         $wasSuccessfull = $account->register($username, $firstName, $lastName, $email, $email2, $password, $password2);

         if($wasSuccessfull){
            $_SESSION['userLoggedIn'] = $username;
            header("Location: index.php");
         }
         
       
    }

?>