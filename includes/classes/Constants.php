<?php 
    class Constants{
        // Register Error Message
        public static $passwordDoNotMatch = "Your passwords don't match";
        public static $passwordNotAlphaNumeric = "Your password can only contain numbers and letters";
        public static $passwordCharacters = "Your password must be between 8 and 30 characters";
        public static $usernameCharacters = "Your username must be between 5 and 25 characters";
        public static $firstNameCharacters = "Your first name must be between 2 and 25 characters";
        public static $lastNameCharacters = "Your last name must be between 2 and 25 characters";
        public static $emailsDonNotMatch = "Your emails don't match";
        public static $emailInvalid = "Email is invalid";
        public static $emailTaken = "This email already been used.";
        public static $usernameTaken = "This username already exists";

        // Login Error Message
        public static $loginFailed = "Username or Password Incorrect";
    }
?>