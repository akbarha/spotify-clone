<?php 
    class Account {
        private $errorArray;
        private $con;
        public function __construct($con){
            $this -> errorArray = array();
            $this -> con = $con;
        }

        public function login($un, $pw){
            // encrypt dan bandingkan password dengan data user yang mau login di table
            $encryptPw = md5($pw);
            $query = mysqli_query($this->con, "SELECT * FROM users WHERE username='$un' AND password='$encryptPw'");

            if(mysqli_num_rows($query) == 1){
                return true;
            }
            else{
                array_push($this->errorArray, Constants::$loginFailed);
                return false;
            }
        }

        public function register($un, $fn, $ln, $em, $em2, $pw, $pw2){
            // Validasi memanggil fungsi private milik sendiri untuk validasi.
            // fungsi register ini akan dipanggil dari register.php karena register public
            $this -> validateUsername($un);
            $this -> validateFirstName($fn);
            $this -> validateLastName($ln);
            $this -> validateEmails($em, $em2);
            $this -> validatePasswords($pw, $pw2);

            // setelah validasi, cek kalau error array kosong berarti account yang dibuat sesuai dengan requirement
            if(empty($this->errorArray)){

                // TODO: Insert ke DB
                // insert ke database
                return $this -> insertUserDetails($un, $fn, $ln, $em, $em2, $pw, $pw2);
            }
            else{
                // gagal karena informasi akun baru tidak sesuai requirement/akun menggunakan username/email yang telah terdaftar
                return false;
            }
        }

        public function getError($error){
            if(!in_array($error, $this -> errorArray)){
                $error = "";
            }
            return "<span class='errorMessage'>$error</span>";
        }

        // Fungsi memasukan data username yang register ke database
        private function insertUserDetails($un, $fn, $ln, $em, $em2, $pw, $pw2){
            $encryptedPw = md5($pw);
            $profilePic = "assets/images/profile-pics/head_emerald.png";
            $date = date("Y-m-d");

            $result = mysqli_query($this -> con, "INSERT INTO users VALUES ('', '$un', '$fn', '$ln', '$em','$encryptedPw', '$date', '$profilePic')");
            return  $result;
        }
        
        // Function Validasi Data Form
        private function validateUsername($un){
            if(strlen($un) > 25 || strlen($un) < 5){
                array_push($this -> errorArray,Constants::$usernameCharacters);
                return;
                // cara panggil error message di sebuah class yang tidakpunya constructor. error message seluruhnya ditaruh di Constants Class
            }

            // TODO: check if username exist in DB
            $checkUsernameQuery = mysqli_query($this->con, "SELECT username from users WHERE username ='$un'");
            if(mysqli_num_rows($checkUsernameQuery) !=0){
                array_push($this->errorArray, Constants::$usernameTaken);
                return;
            }
            // Done
        }
    
        private function validateFirstName($fn){
            if(strlen($fn) > 25 || strlen($fn) < 2){
                array_push($this -> errorArray,Constants::$firstNameCharacters);
                return;
            }
        }
    
        private function validateLastName($ln){
            if(strlen($ln) > 25|| strlen($ln)<2){
                array_push($this -> errorArray,Constants::$lastNameCharacters);
                return;
            }
        }
    
        private function validateEmails($em, $em2){
            if($em != $em2){
                array_push($this -> errorArray,Constants::$emailsDonNotMatch);
                return;
            }

            if(!filter_var($em, FILTER_VALIDATE_EMAIL)){
                array_push($this -> errorArray,Constants::$emailInvalid);
                return;
            }

            //TODO: check if email already been used on DB
            $checkEmailQuery = mysqli_query($this->con, "SELECT email FROM users WHERE email='$em'");
            if(mysqli_num_rows($checkEmailQuery)>0){
                array_push($this->errorArray, Constants::$emailTaken);
            }

        }
    
        private function validatePasswords($pw, $pw2){
            if($pw != $pw2){
                array_push($this -> errorArray, Constants::$passwordDoNotMatch);
                return;
            }

            // Regular Expressions Matching
            
            if(preg_match("/[^A-Za-z0-9]/", $pw)){
                array_push($this -> errorArray,Constants::$passwordNotAlphaNumeric);
            }

            // Passwords Length Check

            if(strlen($pw) > 30 || strlen($pw) < 8){
                array_push($this -> errorArray,Constants::$passwordCharacters);
                return;
            }
        }
    }
?>