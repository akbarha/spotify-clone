<div id="nowPlayingBarContainer">
    <!-- Now Playing Bar Flex -->
    <div id="nowPlayingBar">

        <div id="nowPlayingLeft">

            <div class="content">
                <span class="albumLink">
                    <img class="albumArtwork" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Red_square.svg/240px-Red_square.svg.png" alt="">
                </span>

                <div class="trackInfo">
                    <span class="trackName">
                        <span>Happy Birthday</span>
                    </span>

                    <span class="artistName">
                        <span>Reece Kenney</span>
                    </span>
                </div>
            </div>

        </div>

        <!-- nowPlayingCenter Flex Column-->
        <div id="nowPlayingCenter"> 

            <div class="content playerControls">

                <div class="buttons">
                    <button class="controlButton shuffle" title="Shuffle Button">
                        <img src="assets/icons/shuffle.png" alt="Shuffle">
                    </button>

                    <button class="controlButton previous" title="Previous Button">
                        <img src="assets/icons/previous.png" alt="Previous">
                    </button>

                    <button class="controlButton play" title="Play Button">
                        <img src="assets/icons/play.png" alt="Play">
                    </button>

                    <button class="controlButton pause" title="Pause Button" style="display:none;">
                        <img src="assets/icons/pause.png" alt="Pause" >
                    </button>

                    <button class="controlButton next" title="Next Button">
                        <img src="assets/icons/next.png" alt="Next">
                    </button>

                    <button class="controlButton repeat" title="Repeat Button">
                        <img src="assets/icons/repeat.png" alt="Repeat">
                    </button>
                </div>

                <div class="playbackBar">
                    <span class="progressTime current">0.00</span>
                    <div class="progressBar">
                        <div class="progressBarBg">
                            <div class="progress"></div>
                        </div>
                    </div>
                    <span class="progressTime remaining">0.00</span>
                </div>

            </div>

        </div>

        <!--  -->
        <div id="nowPlayingRight">
            <div class="volumeBar">
                <button class="controlButton volume" title="Volume button">
                    <img src="assets/icons/volume.png" alt="Volume">
                </button>

                <div class="progressBar">
                    <div class="progressBarBg">
                        <div class="progress"></div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>