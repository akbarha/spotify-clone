<?php
    require("includes/config.php");

    // session_destroy();

    if(isset($_SESSION['userLoggedIn'])){
        $userLoggedIn = $_SESSION['userLoggedIn'];
    }
    else{
        header("Location: register.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Slotify</title>
    <link href="https://fonts.googleapis.com/css?family=Helvetica+Neue&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>
<body>

    <div class="mainContainer">

        <div id="topContainer">
            <!-- navbar includes (left part of page navbar) -->
            <?php include("includes/navbarContainer.php") ?>
            <!-- navbar includes -->

            <!-- Main View Container (main content) -->
            <div id="mainViewContainer">
                <div id="mainContent">