<!-- include header -->
<?php include("includes/header.php")?>
<!-- include header -->

    <!-- code above footer and below header are inside mainViewContainer>mainView div-->

    <h1 class="pageHeadingBig">You might also Like</h1>
    
    <div class="gridViewContainer">
        <?php
        // con is inside config.php file that is included inside header.php
            $albumQuery = mysqli_query($con, "SELECT * FROM albums ORDER BY RAND() LIMIT 10");

            while($row = mysqli_fetch_array($albumQuery)){
                echo "<div class='gridViewItem>
                    <img src='{$row['artworkPath']}'>
                    <div class='gridViewInfo> 

                    </div>'
                </div>";
            }
        ?>
    </div>

<!-- include footer -->
<?php include("includes/footer.php") ?>
<!-- include footer -->
